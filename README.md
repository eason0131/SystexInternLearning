# 目錄

### Code: 練習、未完成的程式碼
### Notes: 學習過程筆記
### Project: 陸達通運運輸管理系統專案(未完成)

##

# 工作日誌

## 2021/03/04 Git
* 安裝Git
* 使用GitLab
    * 用 CommandLine 上傳、下載檔案
* 安裝Xcode
* 安裝Visual Studio Code(有terminal功能)


## 2021/03/11 Docker MySQL
* 安裝Docker
* 在Docker上安裝MySQL
    * 在本機端的Workbench上連接Docker上的MySQL (port要小心)
    * 用CommandLine執行Docker MySQL


## 2021/03/18 圖書館查詢系統
* 在Docker上安裝Nginx
* 在Docker Nginx上運行html
    * 簡單html用法


## 2021/03/25 圖書館查詢系統
* 透過線上模板 拼湊一個簡易的圖書館查詢系統
    * 使用html CSS JavaScripts
    * 頁面 登入
    * 頁面 查詢篩選
    * 頁面 動態增加刪除
    * **困難**：如何將資料傳到另一個html - 傳送時url會有亂碼問題
* **AWS EC2 INSTANCE Ubuntu 架設 Git Docker Nginx**


## 2021/04/01 前後端介接
* NVM NPM Node.js
* JSON server | Restful API
    * 假的動態資料庫
* Spring Boot
    * Maven


## 2021/04/08 前後端介接
* Spring Boot
    * Dependency
* MongoDB 安裝


## 2021/04/15 前後端介接 專案提案
* Spring Boot
    * 檔案架構
    * Dependency 建立與增加
    * 連接MySQL(JDBC) 尚未完成
* 專案提案 - 訂單管理系統
    * 系統內容
    * 系統流程圖
    * Note: 一開始先將所有可能需求條列出來，後面再收斂


## 2021/04/29 前後端介接 專案提案
* Spring Boot
    * 檔案架構
* 專案提案 - 訂單管理系統
    * 討論分工


## 2021/05/06 前後端介接 專案提案
* Spring Boot
    * 檔案架構(主程式位置要放對！)
    * 連接MySQL
* 專案提案 - 訂單管理系統
    * 需求訪談
    * 運輸管理系統流程化
    * 初步討論欄位


## 2021/05/13 前後端介接 專案提案
* Spring Boot
    * 連接MySQL
* 專案提案 - 運輸管理系統
    * 文件化
