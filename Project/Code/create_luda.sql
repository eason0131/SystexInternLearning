create database luda;

use luda;

create table users(  
account varchar(300) primary key,  
password varchar(300) not null,
u_name varchar(300),
r int
)ENGINE=InnoDB;

create table role(  
r int primary key,  
account varchar(300)
)ENGINE=InnoDB;

create table vehicle(  
v_number varchar(300) primary key
)ENGINE=InnoDB;

create table order_history(  
o_number varchar(300),
edit_time datetime,
u_name varchar(300),
brief_intro varchar(300),
detail varchar(300)
)ENGINE=InnoDB;

create table order_infomation(  
o_number varchar(300) primary key,
o_status varchar(300),
o_release int,
o_devaning int,
o_damaged int,
o_remark varchar(300),
unloading varchar(300),
export_so varchar(300),
pieces varchar(300),
o_price varchar(300),
o_price_other varchar(300),
weight varchar(300),
volume varchar(300),
origin  varchar(300),
destination  varchar(300),
c_name varchar(300),
v_price varchar(300),
v_class varchar(300),
v_number varchar(300),
eta varchar(300),
call_time date
)ENGINE=InnoDB;

