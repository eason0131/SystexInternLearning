# Ubuntu Docker 安裝 | [Install Docker Engine on Ubuntu](https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository)

**官網安裝方式**

`sudo apt-get update`
`sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release`

`curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg`

`echo \
  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null`

`sudo apt-get update`
`sudo apt-get install docker-ce docker-ce-cli containerd.io`

`apt-cache madison docker-ce`

`sudo apt-get install docker-ce=5:20.10.0~3-0~ubuntu-focal docker-ce-cli=5:20.10.0~3-0~ubuntu-focal containerd.io`

`sudo docker run hello-world`



# Mac Docker安裝

`docker run -d -p 80:80 docker/getting-started`
在終機端輸入此指令


有關Docker [Docker 是什麼？實戰手札帶你認識 Docker](https://tw.alphacamp.co/blog/docker-introduction)


