# Mac重灌
[教你如何免 USB 開機碟，就能重灌乾淨的 Mac 系統](https://today.line.me/tw/v2/article/YNQDGP)

進入[照片] -> 點選[偏好設定] -> 點選[icloud] -> 取消同步 <br />
進入[AppStore] -> 下載[LINE]、[iStatisticaPro] -> 開啟[iStatisticaSensors.dmg] <br />
Google -> 開啟[googlechrome.dmg]、[InstallBackupAndSync.dmg]、[chromeremotedesktop.dmg] <br />
Microsoft -> 開啟[Microsoft_Office_16.46.21021202_Installer.pkg]、[Microsoft_Office_2019_VL_Serializer.pkg] <br />


# Mac儲存空間
[[MacOS]儲存空間裡『其他』類的也太佔硬碟空間了吧？都什麼檔案呀？可以清理掉嗎？](https://km.nicetypo.com/doc/a36b18a776d942987ec54a386ab62f26)


# Mac Windows外接硬碟 | [省錢自製外接Windows開機碟！](https://www.youtube.com/watch?v=imEA-JQKXvk) | [紀錄](https://www.youtube.com/watch?v=fd9yawOiaiI)
* _**Windows電腦**_
    * 插上: 外接硬碟
    * 準備: Windows.iso、WinToUSB
        * 格式化外接硬碟
        * 使用WinToUSB安裝Windows.iso，並選擇"適用於UEFI的GPT"

* _**Mac電腦**_
    * 插上: USB
        * 使用"啟動切換輔助程式"，在上方工具列點選"動作"，"下載Windows支援軟體"(驅動程式)，並將檔案儲存在USB

### 完成前置作業，關閉Mac電腦

* _**Mac電腦**_
    * 插上: 外接硬碟、USB
    * 準備: USB鍵盤滑鼠
        * 按著"option鍵"開機 (可能要做兩次)
        * 使用USB鍵盤滑鼠，安裝Windows
        * 安裝完成後，開啟USB，進入到"WindowsSupport>>>BootCamp>>>Setup"，點選Setup應用程式，並開始安裝
        * 安裝完成後，重新開機即完成

<br />

### 外接硬碟的SSD | [各項裝置的傳輸規格及速度](https://sites.google.com/site/quietlift/home/chuan-shu-gui-ge?fbclid=IwAR0jEFUfdq7ED-L7fIeCVSjo3KfmPDXvShDIhqewnae57cQ9-YQvs1_hYR0)
* [美光SSD](https://www.crucial.tw/products/ssd)
    * NVMe SSD
        * P2
        * P5
    * SATA SSD

### [Windows 10 改變硬碟磁區空間](https://iqmore.tw/windows-10-resize-disk-partition)

### 風扇
Cooler Master U2<br />
USB升降壓模組 X0172
