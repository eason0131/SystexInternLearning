# [網頁模版 - themewagon網站](https://themewagon.com/)

# [HTML CSS JavaScript 之間的關係](https://www.itread01.com/content/1548514806.html)
HTML是網頁的結構，CSS是網頁的樣式，javascript是網頁的行為。
****

# HTML課程 | [w3schools](https://www.w3schools.com/html/)

[!DOCTYPE html的意思](https://matthung0807.blogspot.com/2019/07/html-html_7.html) : 會因版本不同，有不同的宣告語法

## HTML PRACTICE
 | [HTML 基礎](https://developer.mozilla.org/zh-TW/docs/Learn/Getting_started_with_the_web/HTML_basics)
 | [HTML中的圖片](https://developer.mozilla.org/zh-TW/docs/Learn/HTML/Multimedia_and_embedding/Images_in_HTML)

![image](htmlPractice.png)


# JavaScripts課程

## 傳送URL編碼問題 | [JavaScript 頁面跳轉並傳值,解析url成物件Script](https://www.itread01.com/p/982170.html)
decodeURI()——解碼某個編碼的 URI。 <br />
decodeURIComponent()——解碼一個編碼的 URI 元件。 <br />
encodeURI()——把字串編碼為 URI。 <br />
encodeURIComponent()——把字串編碼為 URI 元件。 <br />
