# Spring Boot 用 docker-compose.yml 建置Docker
`cd /Users/eason/Desktop/luda_back/src/main/resources/doc` 到yml檔的位置

`docker-compose -f ./docker-compose.yml -p luda up -d` 執行yml檔
****

`sudo lsof -i:3306`
查詢 port 3306

`sudo kill [PID]`
[清除系統上被佔用的Port](https://andy6804tw.github.io/2018/02/28/kill-port-macos/)
****
****
****



# Spring Boot 建立
* [【JAVA】從0創建SpringBoot+MAVEN項目使用 IntelliJ IDEA](https://medium.com/@rorast.power.game/java-從0創建springboot-maven項目使用-intellij-idea-69dfbc55953d)
* [Spring Boot-第1課-建立初始專案](https://medium.com/chikuwa-tech-study/spring-boot-第1課-建立初始專案-214ff084f787)
* [Spring Boot 教學系列 - 如何製作自己的Spring Boot Starter Library](https://www.youtube.com/watch?v=cGXBg81okew)

## dependency 建立
### dependency 初始化 | [Spring Initializer](https://start.spring.io)
使用 SpringBootInitializer Tool 生成 pom.xml，否則dependency很容易出錯
### dependency 增加 | [Maven Repository](https://mvnrepository.com)
使用 Maven Repository 生成，再貼至 pom.xml


****
# Spring Boot 連接 MySQL

[[Day 4] – 見證Hello World的時刻](https://ithelp.ithome.com.tw/articles/10214500)


## 檔案架構 | [[Day14] – Spring Boot 與MySQL數據庫的應用教學](https://ithelp.ithome.com.tw/articles/10217667)
**主程式：要放在最上層，才能往同層或是下層資料夾去找檔案。**<br>
![image](SpringBoot檔案結構.png)

**pom.xml**: 加入**dependency**

**model**: 負責資料的收集或提取

**repository**: 負責與數據庫溝通，操作CRUD

**service**: 負責簡化代碼，執行repository中的CRUD

**controller**: 負責做各種mapping (仍不太理解)

**Application**:

* Note: @ComponentScan  @EntityScan 在SpringBootApplication就有這些功能了？


## RequestMapping | [Spring 教學(2) - 關於@RequestMapping](http://peaceful-developer.blogspot.com/2014/08/spring-2-requestmapping.html)
以URL作為對應條件
> @RequestMapping("/hello")<br>
> public @ResponseBody String hello(){<br>
>     return "Hello World!";<br>
> }<br>

url="/"，那就是根目錄<br>
* Note: 注意主程式所在位置！不然會找不到檔案！


## 資料庫建立連接 - JdbcTemplate   (另外一種是: Mybatis) | [Spring Boot系列（六）Spring Boot 連接MySql資料庫](https://kknews.cc/zh-tw/code/voko3j2.html)
dependency  application.properties  controller  配置


## 遇到錯誤
> ### SpringBoot項目，Debug啟動錯誤
> Failed to retrieve application JMX service URL
>> [Solution](https://blog.csdn.net/qiaoqi17/article/details/107855826):<br>
>> 到Edit Configuration<br>
>> 取消Enable JMX agent選項


> ### spring-boot入口實現SpringBootServletInitializer介面
> 補充：SpringBootServletInitializer介面依賴javax.servlet包，需要在pom.xml中引入該包<br>
> [使用外部容器執行spring-boot專案：不使用spring-boot內建容器讓spring-boot專案執行在外部tomcat容器中](https://codertw.com/程式語言/634787/)


> ### 啟動SpringBoot失敗
> No active profile set, falling back to default profiles: default
>> [Solution](https://www.cnblogs.com/jpfss/p/10765636.html):<br>
>> 在pom.xml加dependency<br>
>> 加入springcloud的dependency
> ****
> Error creating bean with name 'bootstrapImportSelectorConfiguration': Initialization of bean failed; nested exception is 
> java.lang.NoClassDefFoundError: org/springframework/boot/context/properties/ConfigurationPropertiesBean
>> [Solution](https://cloud.tencent.com/developer/ask/230286):<br>
>> springboot的版本和springcloud的版本不匹配導致。
>> springboot版本用2.2.5.RELEASE  springcloud版本用Hoxton.SR3


> ### 連接MySQL報錯：Config設定
> Loading class `com.mysql.jdbc.Driver'. This is deprecated. The new driver class is `com.mysql.cj.jdbc.Driver'. The driver is automatically 
> registered via the SPI and manual loading of the driver class is generally unnecessary.
>> [Solution](https://medium.com/appxtech/my-sql-報錯the-new-driver-class-is-com-mysql-cj-jdbc-driver-24c7a360346c):<br>
>> 在application.properties<br>
>> 改成com.mysql.**cj**.jdbc.Driver


> ### 連接MySQL報錯：Config設定
> WARN JpaBaseConfiguration$JpaWebConfiguration : spring.jpa.open-in-view is enabled by default. 
> Therefore, database queries may be performed during view rendering. Explicitly configure spring.jpa.open-in-view to disable this warning
>> [Solution](https://blog.csdn.net/qq_35981283/article/details/88994092):<br>
>> 在pom.xml中，加入<br>
>> 改spring.jpa.open-in-view=false


> ### 連接MySQL報錯：Config設定
> Failed to configure a DataSource: 'url'
>> [Solution](https://kknews.cc/zh-tw/code/j9pmome.html):<br>
>> 在pom.xml中，加入<br>
>> druid.driver=com.mysql.jdbc.Driver<br>
>> druid.url=jdbc:mysql://localhost:3306/data?<br>
>> serverTimezone=Asia/Shanghai&allowMultiQueries=true<br>
>> druid.username= root<br>
>> druid.password= root


> ### 連接MySQL報錯：Config設定
> Failed to configure a DataSource: 'url' attribute is not specified and no embedded datasource could be configured.
>> [Solution](https://stackoverflow.com/questions/51221777/failed-to-configure-a-datasource-url-attribute-is-not-specified-and-no-embedd):<br>
>> 在pom.xml中，加入<br>
>> spring.datasource.url=jdbc:mysql://localhost:3306/db
>> spring.datasource.username=
>> spring.datasource.password=   
>> spring.datasource.driver-class-name=com.mysql.jdbc.Driver
>> spring.jpa.database-platform = org.hibernate.dialect.MySQL5Dialect
>> spring.jpa.generate-ddl=true
>> spring.jpa.hibernate.ddl-auto = update
