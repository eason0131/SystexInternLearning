# JSON server 安裝 | [JSON Server](https://www.npmjs.com/package/json-server)

[用 JSON Server 模擬 RESTful API](https://medium.com/@debbyji/用-json-server-模擬-restful-api-f07abda3927c)<br />
`npm install -g json-server` 全域安裝 json-server

`json-server db.json` 開啟 json server，並且指定使用哪個檔案



## JSON 模擬器 | [jsbin](https://jsbin.com/?html,output)



# JSON

每次使用JSON的資料，都需要開啟JSON Server<br />
port 3000: data.json<br />
port 8080: 執行頁面<br />

**JSON 新增 修改 刪除 查詢** -> [Node.js 操作 JSON 文件](https://medium.com/samumu-clan/node-js-操作-json-文件-8aab0bcdc057)


# CRUD

C: POST
R: GET
U: PUT PATCH
D: DELETE

GET：取得(想要的服務)的資料或是狀態。（safe & idempotent）<br />
POST：新增一項資料。<br />
PUT：利用更新的方式於"指定位置"新增一項資料。 （idempotent）<br />
PATCH：在現有的資料欄位中，增加或部分更新一筆新的資料。<br />
DELETE：指定資料刪除。 （idempotent）<br />
