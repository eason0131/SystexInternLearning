**Docker安裝MySQL** [Docker安裝MySQL](https://www.runoob.com/docker/docker-install-mysql.html)

`docker pull mysql:8`

**出現Error: unable to get absolute bin path: getwd: invalid argument**

**注意：要在/Users/eason才能做**

`docker image ls`
查看現有的映像檔


> `docker run -d -v ~/mysql-docker-data:/var/lib/mysql -it --name mysql8 -p 3307:3306 -e MYSQL_ROOT_PASSWORD=12345 -e MYSQL_DATABASE=mydb mysql:8 --default-authentication-plugin=mysql_native_password`
本機端的"3307" 對到 Docker的"3306"(DockerMySQL預設的port)
[使用docker run 啟動](https://matthung0807.blogspot.com/2020/08/docker-mysql.html)


> `docker exec -it mysql8 bash`
進入容器 [mac下利用docker](https://zhuanlan.zhihu.com/p/35272017) | [Docker玩轉MySQL](https://myapollo.com.tw/zh-tw/docker-mysql/)

---------------------------------------------------------------------------------------------------------------------------------

`sudo lsof -i:3306`
查詢 port 3306

`sudo kill [PID]`
[清除系統上被佔用的Port](https://andy6804tw.github.io/2018/02/28/kill-port-macos/)

---------------------------------------------------------------------------------------------------------------------------------

`docker exec -it mysql8 bash`
進入mysql8容器 [Docker安装mysql](https://zhuanlan.zhihu.com/p/302829005)

`mysql -u root -p`
登入mysql服務

[MySQL相關設定筆記](https://www.zeusdesign.com.tw/article/19-Mac%20OS%20上安裝%20MySQL%20以及相關設定筆記.html) ｜ [MySQL指令](https://blog.xuite.net/towns/hc/65849335-MySQL+指令、語法及管理)

=================================================================================================================================

`sudo -i`
以root身份進入/usr/local/mysql/data去刪除不要的東西




# MySQL學習
[匯出／匯入（備份）完整 MySQL 資料庫教學](https://mnya.tw/cc/word/1395.html)
