# Docker 建立 Nginx : | [Docker 建立 Nginx 基礎分享](https://medium.com/@xroms123/docker-建立-nginx-基礎分享-68c0771457fb)<br />
**1. Docker指令 建立 Nginx**<br />
**2. Dockerfile 建立 Nginx**<br />

## 以下為Docker指令 建立 Nginx
### _STEP1:_ 建立一個index.html檔案
在本機端任意檔案夾`ex. /Users/eason/DockerNginxSystex`建立一個`index.html`檔案<br />
**index.html檔案內容：**
```
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Docker nginx</title>
</head>
<body>
  Hello world!
</body>
</html>
```
<br />

### _STEP2:_ 下載Nginx image
`docker pull nginx`<br />
<br />

### _STEP3:_ 創建並啟動Nginx container
`docker run --name [container名稱] -p 8080:80 -v [index.html所在路徑]:/usr/share/nginx/html -d nginx`<br />
> `docker run`：創建docker container的指令<br />
> `--name [container名稱]`：container名稱<br />
> `-p 8080:80`：將local 8080 port 指向 container的 80 port **(放在container port 80 [不能變！])**<br />
> `-d nginx`：選擇container要裝的image(在這裡要裝的是nginx)<br />
> `-v [index.html所在路徑]`：ex. /Users/eason/DockerNginxSystex<br />
<br />

### _STEP4:_ 打開瀏覽器 http://localhost:8080
