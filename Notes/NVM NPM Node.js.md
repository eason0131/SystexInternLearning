# NVM NPM Node.js 關係 | [NPM、NVM 自己的版本自己控制](https://ithelp.ithome.com.tw/articles/10230877)
* NVM(Node Version Manager): 管理自己 Node 版本的工具
* NPM(Node Package Manager): node包管理器



# NVM 安裝 | [Mac电脑 使用 nvm 管理 node 版本](https://www.youtube.com/watch?v=n07AM2iWuG8)
[Google搜尋githubnvm](https://github.com/nvm-sh/nvm#install--update-script) 接著進入Installation

打開終端機 輸入一下程式碼<br />
`curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash`<br />

複製結果的最後兩行<br />
`export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm`

進入編輯器<br />
`vi ~/.bash_profile` (路徑是在/Users/eason)<br />
並貼上
```
PS1=" -> "
PS!="\n$PS1"

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
```
重新開啟終端機，並輸入`nvm`



# Node.js 安裝 | [[ Alex 宅幹嘛 ] 👨‍💻 JSON 與 RESTFUL API 串接一次搞定](https://www.youtube.com/watch?v=R8GL5y49iJc)
`nvm install node`<br />
`nvm install v8.9.4` (要安裝LTS>>LongTermSupport)<br />



# Node.js 實戰 | [Node.js 操作 JSON 文件](https://medium.com/samumu-clan/node-js-操作-json-文件-8aab0bcdc057)
用Node.js進行 **【新增】【刪除】【修改】【查詢】**

`node test.js` 執行node.js
