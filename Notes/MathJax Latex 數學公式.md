# [在線LaTeX公式编辑器](https://www.latexlive.com/##)



# [MathJax：讓web前端支持Latex數學公式](https://www.twblogs.net/a/5e9b35e16052e1406bdd0b2c)

引入MathJax

【方式1】：直接在需要顯示latex公式的html頁面引用cdn

`<script src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/MathJax.js?config=TeX-AMS_HTML" async></script>`


### [使用 MathJax 把 LaTeX 或 MathML 數學式子放進網頁](https://blog.gtwang.org/web-development/mathjax-latex-mathml/)

## Latex在HTML使用上的注意事項 | [數學、歷史、LaTeX](http://cthsueh.blogspot.com/2016/01/latex-mathjax.html)

其他使用上的注意事項寫在下面
* 因為 LaTeX 的語法會被自動轉成數學式, 如果想顯示語法本身如 $\displaystyle F(x)=\int_a^x f(t)dt$ 而不是 F(x)=∫xaf(t)dt 的話, 需用 HTML 的指令 <code>...</code> 夾住。
* 自己定義的 LaTeX 語法將無法使用。
* 環境指令 \begin{}...\end{} 的指令無法使用。

[Tables Generator](http://www.tablesgenerator.com/)


## [12 - Mathematical Equations](https://www.w3.org/MarkUp/HTMLPlus/htmlplus_45.html)



## Latex 語法
[[翻译] LaTeX 中的积分、求和、极限](https://blog.csdn.net/xovee/article/details/107733398)

[常用数学符号的 LaTeX 表示方法](http://mohu.org/info/symbols/symbols.htm)

[Latex基本表格繪製](https://www.twblogs.net/a/5b7cdb112b71770a43dce38a)

[Table Generator](https://www.tablesgenerator.com/latex_tables)


## 其他

* [MikTex + TeXstudio 配置Win10下的latex環境](https://iter01.com/577334.html)


* [題庫 系統分析與設計](https://ir.nctu.edu.tw/bitstream/11536/65279/7/352507.pdf)




## [Try pandoc!](https://pandoc.org/try/)
