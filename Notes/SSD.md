# 格式

**APFS(Apple File System):** macOS High Sierra的新功能。它针对固态硬盘（SSD）和其他全闪存存储设备进行了优化，但也可用于机械和混合驱动器。

**Mac OS Extended(HFS Plus/HFS+):** 老版本的macOS在所有的驱动器上默认使用它。

**ExFAT:** 最好的跨平台选项，专为在Windows和MacOS系统上工作而设计。 将其用于可插入两种计算机的外部驱动器 。

****
Mac OS 擴充格式（日誌式）：Mac 硬碟格式（又或稱日誌式 HFS Plus），一般都是用這個格式。

Mac OS 擴充格式（日誌式、已加密）：除了基本使用 Mac 的格式外，在裝置接上電腦後，要開啟裝置必須輸入一開始設定好的密碼才能使用。

Mac OS 擴充格式（區分大小寫、日誌式）：使用 Mac 格式且檔案和檔案夾名稱有區分大小寫。例如，名稱為「Homework」和「HOMEWORK」的檔案夾會是兩個不同的檔案夾，基本上用不太到這格式。
Mac OS 擴充格式（區分大小寫、日誌式、已加密）：使用 Mac 格式並檔案夾名稱有區分大小寫，在連接裝置後會要求輸入密碼才能使用。

MS-DOS（FAT）：用於容量 32GB 或更小的 Windows 卷宗。

ExFAT：用在容量超過 32 GB 的 Windows 卷宗。


# 架構

GUID 分割區配置表：用於目前所有 Intel 基準的 Mac 電腦。

主開機記錄：用於將會格式化為「MS-DOS（FAT）」或「ExFAT」的 Windows 分割區。

Apple 分割區配置表：用於較舊 PowerPC 基準的 Mac 電腦相容性。
