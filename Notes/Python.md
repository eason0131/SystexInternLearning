# 爬蟲

## 台灣證券交易所
[Python網路爬蟲 抓取台灣證券交易所交易紀錄](https://www.youtube.com/watch?v=IqrFMiJfHBU)


[在 R 中合併兩個 Data Frame](https://www.delftstack.com/zh-tw/howto/r/combine-two-dataframes-in-r/)






## 安裝matplotlib
到AnacondaPrompt下指令
`conda install matplotlib`

## Windows 安裝 Pytorch
[【PyTorch教程】P1. PyTorch環境的配置及安裝](https://medium.com/@zhou.lawliet/pytorch%E6%95%99%E7%A8%8B-p1-pytorch%E7%92%B0%E5%A2%83%E7%9A%84%E9%85%8D%E7%BD%AE%E5%8F%8A%E5%AE%89%E8%A3%9D-c40d6a395531)
### 要記得換Anaconda環境: jupyter notebook導入方法 | [Windows系统CPU版本pytorch安装教程（配置到jupyter notebook和pycharm中](https://blog.csdn.net/monkeyll17/article/details/114884555)

## Mac 安裝 Pytorch
[Mac OS 安裝PyTorch](https://rick.web.nctu.edu.tw/pytorch/)
pip install cmake
