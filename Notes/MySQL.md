# 時區

[mysql時間型別timestamp知識點](https://www.itread01.com/content/1550036889.html)

[關於時區(一)](https://medium.com/@justinlee_78563/%E9%97%9C%E6%96%BC%E6%99%82%E5%8D%80-4bfada7bd23d)

[PHP 取得 MySQL 時間與系統時間差八小時的解法](https://blog.xuite.net/jyoutw/xtech/47098613)


`CREATE TABLE ttt (t timestamp);`

`INSERT INTO ttt VALUES( DATE_ADD(NOW(),INTERVAL 8 HOUR) )`


# 資料庫隨機選取幾個

[隨機讀取N條記錄](https://www.itread01.com/content/1547591613.html)

[從 SQL 資料庫中隨機選取資料](https://dotblogs.com.tw/johnny/2010/01/18/13079)


`---隨機抽題目 選三個`

`use questionTest;`

`Select * From question Order By Rand() Limit 3`
