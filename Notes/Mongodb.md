# Mongodb 安裝 | [How to install MongoDB on Mac](https://www.youtube.com/watch?v=DX15WbKidXY)

到下載檔(.tar)的目錄下

`tar xzf mongodb-macos-x86_64-4.4.4.tar` 解壓縮

`sudo mv mongodb-macos-x86_64-4.4.4 /usr/local/mongodb` 將安裝包移至/usr/local/mongodb

`cd /usr/local/mongodb/`

****

以下參考 [mkdir /data/db 出錯](https://stackoverflow.com/questions/58034955/read-only-file-system-when-attempting-mkdir-data-db-on-mac)
`brew tap mongodb/brew`

`brew install mongodb-community@4.2`

`brew services start mongodb-community@4.2`

`ps aux | grep -v grep | grep mongod`

****

把以下都打開來跑一次 (以下檔案在/usr/local/mongodb)

/Users/eason/Desktop/mongodb/bin/install_compass

/Users/eason/Desktop/mongodb/bin/mongo

/Users/eason/Desktop/mongodb/bin/mongod

/Users/eason/Desktop/mongodb/bin/mongos 
