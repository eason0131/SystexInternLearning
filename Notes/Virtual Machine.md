# Virtual Machine

# VirtualBox install Windows10 on Mac

[安裝的架構](https://iter01.com/460776.html)<br >
[安裝示範影片](https://www.youtube.com/watch?v=HEL9Uw3AR9s)

## 出現問題
- [VirtualBox 在 macOS 出現 Kernel driver not installed 問題解決方式](https://officeguide.cc/virtualbox-macos-kernel-driver-not-installed-error-solution-2020/)

- 無法選擇安裝的ISO檔
    > 解法: <br>
    > VirtualBox管理員 選擇 Windows10(虛擬機) > 設定 > 存放位置<br> > 選擇 空的 > 按下 光碟機後的按鈕 > 選擇磁碟檔... > 確認 > 開啟 Windows10(虛擬機)

- 安裝guest additions



# VMWare

[VMware Fusion 12 個人用序號免費拿，Mac 用戶安裝 Windows 10 虛擬系統必拿](https://www.kocpc.com.tw/archives/346202)

[Mac 安装 VM 虚拟机，搭载 Ubuntu 系统](https://cloud.tencent.com/developer/article/1652286)

[Day 21 : Linux - 安裝ubuntu的時候視窗太小，按不到下方的繼續鍵怎麼辦？](https://ithelp.ithome.com.tw/articles/10277696?sc=hot)
