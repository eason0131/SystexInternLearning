# Git 注意
Git需要搭配Xcode | [出现xcrun: error: invalid active developer path, missing xcrun的问题](https://www.jianshu.com/p/50b6771eb853)


# Ubuntu Git 安裝
`sudo apt-get install git-all`





# 設定Git | [很基礎的 Command Line 與 Git](https://claygao.coderbridge.io/2020/03/19/-學習筆記系列--git-與-command-line/) | [初次設定Git](https://git-scm.com/book/zh-tw/v2/開始-初次設定-Git)｜[Git基本使用](http://blog.gogojimmy.net/2012/01/17/how-to-use-git-1-git-basic/)

`git config --global user.name "xxx"`
設定user的name

`git config --global user.email "xxx@gmail.com"`
設定user的email

`git config --list`
列出Git設定



# PushToGitLab | [Gitlab基本用法](https://rommelhong.medium.com/七分鐘學會gitlab-ecdcbcb42b9c)

`git init`
Git初始化

`git add .`
將所有資料加入暫存(Stage)

`git commit -m "xxx(ex. Initial)"`
提交版本(Commit)

`git remote add [name(ex. origin)] [url]`
給這串URL(Project>Clone>HTTPS)一個簡單的別名(ex. origin) (ex. php(schoolProject))

`git push [repository(ex. origin)] [refspec(branch)(ex. master)]`
把檔案push上去
專案結構：樹的主軸master、樹的分支branch



## PushToGitLab錯誤及解決辦法

```
error:src refspec master does not match any
```

_原因分析：引起該錯誤的原因是目錄中沒有檔案，空目錄是不能提交上去的_

_解決辦法：_ [參考來源](https://www.itread01.com/content/1546763944.html)

`touch README`

`git add README`

****

```
remote: HTTP Basic: Access denied
fetal: Authentication failed for 'https://gitlab.com/eason0131/gitlablearning.git/'
```

_原因分析：很明顯，就是遠端服務端的使用者名稱和密碼與當前系統中git儲存的使用者名稱和密碼有衝突_

_解決辦法：_ [參考來源](https://www.itread01.com/content/1543946774.html)

在作業系統中以管理員身份開啟命令列工具

`git config --system --unset credential.helper`

****

```
remote: HTTP Basic: Access denied
fetal: Authentication failed for 'https://gitlab.com/eason0131/gitlablearning.git/'
```

_原因分析：Gitlab沒有設定密碼_

_解決辦法：_ [組員]()

到Gitlab中去設定密碼，就有權限了



# PushToLocal

`git clone https://...`
