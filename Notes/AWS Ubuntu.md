# 在AWS架設EC2 Ubuntu | [[AWS][教學] AWS基本使用#03. 建立EC2 instance(Ubuntu)](https://www.youtube.com/watch?v=kOHiDHb38MU)

# 如何進入 Ubuntu 主機？
    - 將 **demo_progressbar.pem** 鑰匙放到 **Desktop**
    - 本機終機端到 **Desktop**
    $ `chmod 400 demo_progressbar.pem`
    $ `ssh -i "demo_progressbar.pem" ubuntu@ec2-13-231-165-7.ap-northeast-1.compute.amazonaws.com`


## 進到 Ubuntu 主機
* `sudo apt install net-tools`
* `ifconfig`
發現public ip 跟主機ip不一樣(inet 主機ip)

* `sudo -i`

* 建立使用者
    * `adduser eason`
    * `mkdir /home/eason/Desktop`
    * `cd /home/eason/Desktop`

* 安裝 Git
    * `sudo apt-get install git-all`
    
* 安裝 Docker

* 把檔案clone到Ubuntu

* 安裝 Docker Server (Nginx)
    * **注意：要開AWS上面的port(預設只有22)**



# 安裝XAMPP、phpMyAdmin 遠端連接 | [在 Linux 平台安裝 XAMPP](https://polinwei.com/linux-install-xampp/)
* 將程式碼放到htdocs的資料夾底下
    * `cd /opt/lampp/htdocs/`

* [Linux之Xampp外網訪問不了](https://www.itread01.com/content/1549906234.html)

* 更改文件後，重啟lampp，會遇到的問題
    * 刪除兩個檔案
    * [停止Apache的錯誤](https://stackoverflow.com/questions/23605659/getting-following-error-while-stopping-apache)

* [vi指令](http://linux.vbird.org/linux_basic/0310vi/0310vi.php)

### **網址IP是 公有IPv4地址 ，不是inet**



其餘參考：
[網站部署 - 設定 AWS EC2 遠端主機 + Ubuntu LAMP 環境 + phpMyAdmin](https://hackmd.io/@Heidi-Liu/note-website-deployment)


## Mac XAMPP 路徑路徑 | [Mac 下XAMPP的配置，找不到XAMMP資料夾的來看這個啦](https://www.itread01.com/content/1543309994.html)
* 要先到Volumes按下mount 就會有一個硬碟(IP位置)跑出來 而裡面就有/Applications/XAMPP/xamppfile/htdocs.conf
* 開啟瀏覽器：http://192.168.64.2/php/chapter7/product.php


## 啟動XAMPP
`sudo /opt/lampp/lampp start`
