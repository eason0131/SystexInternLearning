package com.demo.demo.Repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import com.demo.demo.Model.ModelDemo;

import javax.activation.DataSource;

@Repository
public class RepositoryDemo {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public void addStaff(ModelDemo modelDemo) {
        String sql = "INSERT INTO staffinfo " +
                "(PASSWORD, EMAIL, POSITION, PHONE, CREATE_DATE) VALUES (?,?,?,?,NOW())";
        jdbcTemplate = new JdbcTemplate((javax.sql.DataSource) dataSource);
        jdbcTemplate.update(sql, new Object[]{modelDemo.getPassword(),
                modelDemo.getEmail(), modelDemo.getPosition(), modelDemo.getPhone()
        });
    }
}

/*
    //Notice Capital of ModelDemo&modelDemo
    public void addStaff(ModelDemo modelDemo){
        System.out.println("EXCUTE INSERT MEMBER");
        jdbcTemplate.update("INSERT INTO staffinfo(PASSWORD, EMAIL, POSITION, PHONE, CREATE_DATE) " + "VALUES (?,?,?,?,NOW())",modelDemo.getPassword(), modelDemo.getEmail(), modelDemo.getPosition(), modelDemo.getPhone());
    }


}
*/