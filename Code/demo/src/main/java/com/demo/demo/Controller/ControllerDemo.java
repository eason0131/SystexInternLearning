package com.demo.demo.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.demo.demo.Model.ModelDemo;
import com.demo.demo.Service.ServiceDemo;

@RestController
public class ControllerDemo {

    @Autowired
    ModelDemo modelDemo;

    @Autowired
    ServiceDemo serviceDemo;

    @RestController
    public class AddController {
        @RequestMapping(value = "/addStaff")
        public String addStaff(){
            modelDemo = new ModelDemo();
            modelDemo.setPassword("1234");
            modelDemo.setEmail("email@email.com");
            modelDemo.setPhone("22334455");
            modelDemo.setPosition("Manager");
            serviceDemo.addStaff(modelDemo);
            return "New Staff added";
        }
    }


}