package com.demo.demo.Demo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SpringBootHelloWorld {

    public static void main(String[] args) {
        // TODO Auto-generated method stub

    }

    @RequestMapping("/")
    public String hello(){
        return "Hey, Spring Boot 的 Hello World ! ";
    }


    @RestController
    public class GreetingController {
        @RequestMapping(value = "/greeting")
        public String get() {
            return "greeting";
        }
    }

}
