package com.demo.demo.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.demo.demo.Model.ModelDemo;
import com.demo.demo.Repository.RepositoryDemo;

@Service
public class ServiceDemo {

    @Autowired
    RepositoryDemo Repository;
    public void addStaff(ModelDemo ModelDemo){
        Repository.addStaff(ModelDemo);
    }

}