create database patientINFO;
use patientINFO;
create table patient (
	NAME varchar(20),
	ID varchar(11) NOT NULL,
    BIRTHDAY DATE,
    SEX char(1),
    BLOOD varchar(2),
    primary key (ID));
insert into patient values('Amy','A123456789','2000-01-01','F','A');
insert into patient values('Bella','B123456789','2001-02-01','F','B');